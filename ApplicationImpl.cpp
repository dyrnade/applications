/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * IMPL function
 *  getBacktrace collectes the backtrace and demangles the function
 *  names to provide a nice outupt.
 *  This is purely an implementational detail.
 *  This function can be modified/removed at anytime.
 **/


#if __has_include(<execinfo.h>)
#include <execinfo.h>           // for backtrace
#include <cxxabi.h>             // for dladdr
#include <dlfcn.h>              // for __cxa_demangle
#endif

#include <sstream>

#include <QDir>
#include <QDebug>
#include <QStandardPaths>

#include "ApplicationImpl.hpp"

// A C++ function that will produce a stack trace with demangled function and method names.
QString getBacktrace() {
#if __has_include(<execinfo.h>)
    void      *callstack[ 128 ];
    const int nMaxFrames = sizeof(callstack) / sizeof(callstack[ 0 ]);
    char      buf[ 1024 ];
    int       nFrames = backtrace( callstack, nMaxFrames );

    std::ostringstream trace_buf;

    for ( int i = 1; i < nFrames; i++ ) {
        Dl_info info;

        if ( dladdr( callstack[ i ], &info ) ) {
            char *demangled = NULL;
            int  status;
            demangled = abi::__cxa_demangle( info.dli_sname, NULL, 0, &status );
            snprintf(
                buf,
                sizeof(buf),
                "%-3d %*p %s + %zd\n",
                i,
                ( int )(2 + sizeof(void *) * 2),
                callstack[ i ],
                status == 0 ? demangled : info.dli_sname,
                (char *)callstack[ i ] - (char *)info.dli_saddr
            );
            free( demangled );
        }
        else {
            snprintf(
                buf,
                sizeof(buf),
                "%-3d %*p\n",
                i,
                ( int )(2 + sizeof(void *) * 2),
                callstack[ i ]
            );
        }

        trace_buf << buf;
    }

    if ( nFrames == nMaxFrames ) {
        trace_buf << "  [truncated]\n";
    }

    return QString( trace_buf.str().c_str() );

#else
    return QString( "This library/application was compiled without execinfo.h, cannot provide a backtrace!" );

#endif
}


QString ApplicationImpl::getSocketPath() {
    if ( mAppName.isEmpty() ) {
        qDebug() << "Application name is not set. Cannot lock application.";
        return QString();
    }

    if ( mOrgName.isEmpty() ) {
        qDebug() << "Organization name is not set. Cannot lock application.";
        return QString();
    }

    /** Create the socket path */
    QString sockPath( "%1/%2" );
    QString sockDir( "%1/%2/%3" );

    sockDir = sockDir.arg( QStandardPaths::writableLocation( QStandardPaths::RuntimeLocation ) )
                 .arg( mOrgName.replace( " ", "" ) )
                 .arg( QString( qgetenv( "XDG_SESSION_ID" ) ) );

    /** Create this folder */
    if ( not QDir( "/" ).mkpath( sockDir ) ) {
        qDebug() << "Failed to created socket path:" << sockDir;
        return QString();
    }

    sockPath = sockPath.arg( sockDir ).arg( mAppName.replace( " ", "" ) );

    return sockPath;
}

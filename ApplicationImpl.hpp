/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * IMPL function
 *  getBacktrace collectes the backtrace and demangles the function
 *  names to provide a nice outupt.
 *  This is purely an implementational detail.
 *  This function can be modified/removed at anytime.
 **/

#pragma once

#include <QString>

namespace DFL {
    namespace IPC {
        class Server;
        class Client;
    }
}

class QLockFile;

/**
 * Pretty-format the backtrace.
 * This function demangles the namespaces and prettifies
 * the backtrace.
 */
QString getBacktrace();

class ApplicationImpl {
    public:
        QLockFile *lockFile = nullptr;

        DFL::IPC::Server *mIpcServer = nullptr;
        DFL::IPC::Client *mIpcClient = nullptr;

        QString mAppName;
        QString mOrgName;

        /**
         * Get the socket path.
         * This function divines the socket path from app name,
         * org name and XDG_SESSION_ID.
         */
        QString getSocketPath();
};

/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * DFL::Application is a thin wrapper around QApplication
 * to provide a single instance of an application.
 **/

#include <signal.h>
#include <unistd.h>

#include "Application.hpp"
#include "ApplicationImpl.hpp"

#include <IpcServer.hpp>
#include <IpcClient.hpp>


DFL::Application::Application( int& argc, char **argv ) : QApplication( argc, argv ) {
    impl = new ApplicationImpl;
}


DFL::Application::~Application() {
    disconnect();
}


void DFL::Application::setApplicationName( QString name ) {
    impl->mAppName = name;
    QApplication::setApplicationName( name );
}


void DFL::Application::setOrganizationName( QString name ) {
    impl->mOrgName = name;
    QApplication::setOrganizationName( name );
}


bool DFL::Application::lockApplication() {
    /** If another instance is already running, we will not even attempt to lock it. */
    if ( isRunning() ) {
        return false;
    }

    QString sockPath = impl->getSocketPath();

    /** Empty sockPath => can't lock application */
    if ( sockPath.isEmpty() ) {
        return false;
    }

    /** Initialize QLockFile */
    if ( not impl->lockFile ) {
        impl->lockFile = new QLockFile( sockPath + ".lock" );
    }

    /** If we fail to lock, may be another instance has passed this stage? */
    if ( not impl->lockFile->tryLock() ) {
        delete impl->lockFile;
        impl->lockFile = nullptr;

        return false;
    }

    /** Create the server instance */
    impl->mIpcServer = new DFL::IPC::Server( sockPath + ".socket", this );

    /** Failed to start the server */
    if ( not impl->mIpcServer->startServer() ) {
        delete impl->mIpcServer;
        impl->mIpcServer = nullptr;

        return false;
    }

    connect( impl->mIpcServer, &DFL::IPC::Server::messageReceived, this, &DFL::Application::messageFromClient );

    /** Everything has gone well. Server started; waiting for clients */
    return true;
}


bool DFL::Application::isRunning() {
    /** This is the FIRST instance. No other instance is running */
    if ( impl->lockFile and impl->mIpcServer ) {
        return false;
    }

    /** If mIpcClient is non-null, then it's connected to the server */
    if ( impl->mIpcClient ) {
        return true;
    }

    QString sockPath = impl->getSocketPath();

    if ( sockPath.isEmpty() ) {
        return false;
    }

    /** Initialize QLockFile */
    if ( not impl->lockFile ) {
        impl->lockFile = new QLockFile( sockPath + ".lock" );
    }

    /** Quick test: If we can lock, then this is the first instance */
    if ( impl->lockFile->tryLock() ) {
        impl->lockFile->unlock();
        return false;
    }

    else {
        return true;
    }

    /** By default, we'll assume this is the first instance */
    return false;
}


void DFL::Application::interceptSignal( int signum, bool autoHandle ) {
    switch ( signum ) {
        case SIGINT:
        case SIGTERM:
        case SIGQUIT: {
            if ( autoHandle ) {
                signal( signum, DFL::Application::autoHandleSignal );
            }

            else {
                signal( signum, DFL::Application::retransmitSignal );
            }

            break;
        }

        /**
         * It's not advised to continue after SIGABRT or SIGSEGV.
         * So we will not do it.
         */
        case SIGABRT:
        case SIGSEGV: {
            signal( signum, DFL::Application::autoHandleSignal );

            break;
        }

        default: {
            qDebug() << "Signal" << signum << "is not intercepted by DFL::Application";
        }
    }
}


bool DFL::Application::messageServer( const QString& message ) {
    if ( not isRunning() ) {
        return false;
    }

    /** Conenct to the client */
    if ( not impl->mIpcClient ) {
        QString sockPath = impl->getSocketPath();

        if ( sockPath.isEmpty() ) {
            return false;
        }

        impl->mIpcClient = new DFL::IPC::Client( sockPath + ".socket", this );

        if ( not impl->mIpcClient->connectToServer() ) {
            return false;
        }
    }

    /** At this point mIpcClient is ready and has started operations */
    if ( not impl->mIpcClient->waitForRegistered() ) {
        return false;
    }

    /** Re-transmit the signal we receive from mIpcClient */
    connect( impl->mIpcClient, &DFL::IPC::Client::messageReceived, this, &DFL::Application::messageFromServer );

    /* Send the message to the server */
    return impl->mIpcClient->sendMessage( message.toUtf8() );
}


bool DFL::Application::messageClient( const QString& message, int fd ) {
    if ( not impl->mIpcServer ) {
        return false;
    }

    return impl->mIpcServer->reply( fd, message );
}


void DFL::Application::broadcast( const QString& message ) {
    impl->mIpcServer->broadcast( message );
}


void DFL::Application::disconnect() {
    /** Disconnect signals and slots */
    QApplication::disconnect();

    if ( impl->mIpcServer ) {
        impl->mIpcServer->shutdown();
        delete impl->mIpcServer;
    }

    if ( impl->lockFile ) {
        impl->lockFile->unlock();

        delete impl->lockFile;
    }

    if ( impl->mIpcClient ) {
        impl->mIpcClient->disconnectFromServer();

        delete impl->mIpcClient;
    }
}


void DFL::Application::autoHandleSignal( int signum ) {
    /** No more contact with the external world. */
    qApp->disconnect();

    switch ( signum ) {
        /** Clean up the temporary files and cease activities immediately. */
        case SIGINT: {
            qInfo() << "The current process has been interrupted. Cleaning up.";
            std::exit( 0 );
        }

        /** Ask the user if they want to save the data, then clean up the state */
        case SIGTERM: {
            qInfo() << "Polite quit request received. Cleaning up.";
            std::exit( 0 );
        }

        case SIGQUIT: {
            qInfo() << "Termination requested via SIGQUIT. Cleaning up.";
            QString bt = getBacktrace();
            write( STDERR_FILENO, bt.toUtf8().constData(), bt.toUtf8().size() );
            fsync( STDERR_FILENO );
            std::exit( 0 );
        }

        case SIGABRT: {
            QString bt = getBacktrace();
            write( STDERR_FILENO, "SIGABRT received. Aborting.... \n", 32 );
            write( STDERR_FILENO, bt.toUtf8().constData(),             bt.toUtf8().size() );
            fsync( STDERR_FILENO );

            std::exit( 0 );
        }

        case SIGSEGV: {
            QString bt = getBacktrace();
            write( STDERR_FILENO, "Segmentation violation occurred. Terminating.... \n", 50 );
            write( STDERR_FILENO, bt.toUtf8().constData(),                               bt.toUtf8().size() );
            fsync( STDERR_FILENO );

            std::exit( 0 );
        }

        default: {
            qDebug() << "Not handing this signal:" << signum;
            break;
        }
    }
}


void DFL::Application::retransmitSignal( int signum ) {
    if ( signum == SIGINT ) {
        emit qApp->interrupted();
    }

    else if ( (signum == SIGTERM) or (signum == SIGQUIT) ) {
        emit qApp->terminate();
    }

    else {
        // Nothing
    }
}
